FROM tensorflow/tensorflow:1.14.0-gpu-py3

WORKDIR /app

RUN pip install --upgrade pip && \ 
    pip install keras==2.0.8 && \
    pip install scipy && \
    pip install scikit-image

CMD ["bash"]